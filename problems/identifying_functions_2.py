"""
Requirements:

Given a filename, read the file and convert from
JSON format to native Python data types.

def read_json_data(filename):

    # open and read file

    # decode json format

    return data
"""

import json
from xml.etree.ElementTree import tostring

# TODO: Complete this function
def read_json_data(filename):
    json_data = None
    with open(filename) as f:
        json_data = f.read()
    if json_data:
        return json.loads(json_data)
    return json_data


read_json_data("test.json")
